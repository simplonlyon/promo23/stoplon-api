<?php

namespace App\Controller;
use App\Entity\Streak;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class CreateStreak extends AbstractController{


    public function __invoke(Streak $streak):Streak {
        $streak->setStart(new \DateTime());
        $streak->setEndDate(null);
        $streak->setOwner($this->getUser());
        
        return $streak;
    }
}