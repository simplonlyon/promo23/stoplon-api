<?php

namespace App\Controller;
use App\Entity\User;
use App\Repository\UserRepository;
use Lexik\Bundle\JWTAuthenticationBundle\Services\JWTTokenManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class RegisterUser extends AbstractController{

    public function __construct(private UserRepository $repo, private JWTTokenManagerInterface $jwt){}

    public function __invoke(User $user):JsonResponse {
        $this->repo->save($user, true);
        
        return new JsonResponse(['token' => $this->jwt->create($user)], 201);
    }
}