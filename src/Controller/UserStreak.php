<?php

namespace App\Controller;
use App\Entity\Streak;
use App\Repository\StreakRepository;
use Doctrine\Common\Collections\Collection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Attribute\AsController;

#[AsController]
class UserStreak extends AbstractController{

    public function __construct(private StreakRepository $repo){}

    public function __invoke():array {
        
        
        return $this->repo->findBy(['owner' => $this->getUser()]);
    }
}