<?php

namespace App\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Post;
use App\Controller\RegisterUser;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ApiResource(
    operations: [
        new Post(controller: RegisterUser::class)
    ]
)]
#[ORM\Entity(repositoryClass: UserRepository::class)]
#[ORM\Table(name: '`user`')]
class User implements UserInterface
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    private ?string $name = null;

    #[ORM\OneToMany(mappedBy: 'owner', targetEntity: Streak::class, orphanRemoval: true)]
    private Collection $streaks;

    

    public function __construct()
    {
        $this->streaks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Streak>
     */
    public function getStreaks(): Collection
    {
        return $this->streaks;
    }

    public function addStreak(Streak $streak): self
    {
        if (!$this->streaks->contains($streak)) {
            $this->streaks->add($streak);
            $streak->setOwner($this);
        }

        return $this;
    }

    public function removeStreak(Streak $streak): self
    {
        if ($this->streaks->removeElement($streak)) {
            // set the owning side to null (unless already changed)
            if ($streak->getOwner() === $this) {
                $streak->setOwner(null);
            }
        }

        return $this;
    }

    public function getRoles(): array{
        return ['ROLE_USER'];
    }

    public function eraseCredentials(){

    }

    public function getUserIdentifier(): string{
        return strval($this->id);
    }
}
